package com.testgit.demo;

import org.springframework.web.bind.annotation.RequestMapping;

public class FirstPageController {
	   @RequestMapping(value = "/")
	   public String firstPage() {
		   System.out.println("First Page");
		   System.out.println("First Page controller");
	      return "first_page";
	   }
}
